module.exports = {
  /*
  ** Headers of the page
  */
  head: {
    title: 'Nuxt-EDgrid',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: 'Nuxt.js project' }
    ],
    link: [
      { rel: 'stylesheet', href: 'https://fonts.googleapis.com/css?family=Lato:400,400i,700|Open+Sans:700' },
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress bar color
  */
  loading: { color: '#00BFF0', height: '5px' },
  /*
  ** Build configuration
  */
  build: {
    /*
    ** Run ESLint on save
    */
    extend (config, { isDev, isClient }) {
      if (isDev && isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    postcss: [
      require('autoprefixer')({
        browsers: ['last 2 versions', 'Firefox ESR', '> 1%', 'ie >= 8', 'iOS >= 8', 'Android >= 4']
      }),
      require('postcss-pxtorem')({
        rootValue: 100,
      })
    ],
  },
  modules: [
    ['nuxt-sass-resources-loader', '@/assets/sass/ed-grid.scss']
  ],
  css: [
    '@/assets/sass/styles.scss'
  ],
  plugins: ['~/plugins/global.js']
}
