import Vue from 'vue'
import AppButton from '~/ed-components/AppButton.vue'
import MainBanner from '~/ed-components/MainBanner.vue'
import FormSelect from '~/ed-components/app-form/FormSelect.vue'
import FormInput from '~/ed-components/app-form/FormInput.vue'
import FormCheckbox from '~/ed-components/app-form/FormCheckbox.vue'
import FormRadio from '~/ed-components/app-form/FormRadio.vue'
import FormTextArea from '~/ed-components/app-form/FormTextArea.vue'

Vue.component('app-button', AppButton)
Vue.component('main-banner', MainBanner)
Vue.component('form-select', FormSelect)
Vue.component('form-input', FormInput)
Vue.component('form-checkbox', FormCheckbox)
Vue.component('form-radio', FormRadio)
Vue.component('form-text-area', FormTextArea)